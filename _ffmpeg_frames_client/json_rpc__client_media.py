#!/usr/bin/env python3 

import requests 
import sys


def __gen_id(pid=None, ts=None):
    import time , os
    if (pid == None ): pid= os.getpid()
    if (ts == None ) : ts = time.time()
    return f"{pid}_{int(ts*1000)}"


def handle_if_error(resp):
    if (resp.json().get('error')!=None):
        print("ERROR      : [{}]".format(resp.json().get('error')) )
        print("TRACEBACK  : [{}]".format(resp.json().get('traceback'))    )
        return True
    return False
    
    

def main():
    import json


    # URL="http://127.0.0.1:8008/jsonrpc"
    URL="http://172.29.76.142:8008/jsonrpc"  # dev-vzstress07-z2
    URL="http://172.29.76.142:8009/jsonrpc"  # dev-vzstress07-z2

    # 'media_data_file_list'       : media_data_file_list         # 
    # 'media_data_remove'          : media_data_remove            # 
    # 'media_camera_playlist_show' : media_camera_playlist_show   # 
    # 'media_camera_playlist_add'  : media_camera_playlist_add    # 
    # 'media_camera_playlist_del'  : media_camera_playlist_del    # 
    # 'media_camera_playlist_set'  : media_camera_playlist_set    # 




    with requests.Session() as s:
        data = { 'jsonrpc': '2.0', 'id': 0 }

        TAG = "[1] media_data_file_list "
        print(TAG)
        print(f"[1] media_data_file_list   ..." )
        method = 'media_data_file_list'
        data.update(   method = method, params = [ (), {} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )

        TAG = "[1.1] media_data_file_list "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_data_file_list'
        data.update(   method = method, params = [ (), {'dir':'240p'} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )



        TAG = "[1.2] media_data_file_list "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_data_file_list'
        data.update(   method = method, params = [ (), {'dir':'240p1'} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        # --------------------------------------------------------------
        # --------------------------------------------------------------
        # Upload FILE / List File / Delete File 
        from  urllib.parse import urljoin
        import glob, os
        
        TAG = "[2.0] Upload FILE  "
        test_media_dir = 'test1/240p'
        print(TAG)
        print(f"{TAG} ..." )
        url  = urljoin(URL, 'media')
        upload_list = [ (test_media_dir, fn)  for fn in glob.glob('./__uplaod_test_data/*.mkv') ]
        for ff in upload_list:
            src_fname = ff[1]
            dst_dir   = ff[0]
            dst_url   = urljoin(URL, 'media/' + os.path.basename( src_fname ) )
            with open(src_fname,'rb') as r:
                resp = s.put(dst_url, data=r, params={'dir':dst_dir} )
            print( f"REQ  : {resp.request.method} --> {resp.request.url} \n  HEADERS: {resp.request.headers}" )
            print( f"RESP : {resp.status_code}    <-- HEADERS: {resp.headers} PAYLOAD: [{resp.text}]" )
        
        # List FILE ------------
        TAG = "[2.1] media_data_file_list "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_data_file_list'
        data.update(   method = method, params = [ (), {'dir':test_media_dir} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = resp.json()['result']
            jresult = json.dumps(result, indent=4)
            print( f"{TAG} result:<--:{jresult}" )
        else :
            raise Error("Got Error")

        hdr = result['hdr']
        remote_filelist_info = [ dict( zip(hdr, r) ) for r in result['data'] ]
        
        # Add File to one camera playlist (WORKS LOCALLY OONLY !!!!) 
        TAG = "[2.2] Add file to camera playlist  "
        print(f"{TAG} ..." )
        method = 'shell'
        remote_fname =  remote_filelist_info[0]['path']
        data.update(   method = method, params = [ 
            '''( 
                  echo `realpath media/data/{remote_fname}` to  "/dev/shm/playlist_2P20A010H3913x01.list" ; 
                  echo `realpath media/data/{remote_fname}` >>  "/dev/shm/playlist_2P20A010H3913x01.list" ; 
               ) '''.format(remote_fname=remote_fname) 
            ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        result = resp.json()['result']
        print( f"{TAG} RESP:<--:{resp.json()}" )
        print(f"RESULT :{result}")
        
        # Delete File
        TAG = "[2.3] media_data_remove "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_data_remove'
        remote_filelist = [ ii['path'] for ii in remote_filelist_info ]
        print( f"{TAG} FILELIST to REMOE :{json.dumps(remote_filelist, indent=4)}" )
        data.update(   method = method, params = [ (remote_filelist,), {} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        

        # --------------------------------------------------------------
        # [3] PLAYLIST manipulations  
        # -------------------------------------------------------------
        TAG = "[3.1] media_camera_playlist_show "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_camera_playlist_show'
        data.update(   method = method, params = [ ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ], {} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        
        # -------------------------------------------------------------------------------
        # [3.0] Get Media_data File List / choose file / Add file to camera(s) playlist ---
        # -------------------------------------------------------------------------------
        TAG = "[3.0] Upload FILE  "
        test_media_dir = 'test1/240p'
        print(TAG)
        print(f"{TAG} ..." )
        url  = urljoin(URL, 'media')
        upload_list = [ (test_media_dir, fn)  for fn in glob.glob('./__uplaod_test_data/*.mkv') ]
        for ff in upload_list:
            src_fname = ff[1]
            dst_dir   = ff[0]
            dst_url   = urljoin(URL, 'media/' + os.path.basename( src_fname ) )
            with open(src_fname,'rb') as r:
                resp = s.put(dst_url, data=r, params={'dir':dst_dir} )
            print( f"REQ  : {resp.request.method} --> {resp.request.url} \n  HEADERS: {resp.request.headers}" )
            print( f"RESP : {resp.status_code}    <-- HEADERS: {resp.headers} PAYLOAD: [{resp.text}]" )
        
        # List FILE -----------------------------
        TAG = "[3.1] media_data_file_list "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_data_file_list'
        data.update(   method = method, params = [ (), {'dir':test_media_dir} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = resp.json()['result']
            jresult = json.dumps(result, indent=4)
            print( f"{TAG} result:<--:{jresult}" )
        else :
            raise Error("Got Error")

        remote_filelist_info = [ dict( zip(result['hdr'], r) ) for r in result['data'] ]
        file_list = [ fn['path'] for fn in  remote_filelist_info ]
        
        print( f"{TAG} file_list :{ json.dumps(file_list, indent=4) }" )
        
        # Get LocalDB Cameras -------------------- 
        TAG = "[3.2.1] localdb_list_cameras " 
        method='localdb_list_cameras'
        data.update( method=method, params=[(),{}], id= __gen_id()  ) 
        resp = s.post( URL, json=data )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        cam_serials  = [ row['cam_serial'] for row in  resp.json()['result'] ]
         
        
        # ADD media to playlist ------------------------- 
        TAG = "[3.3] media_camera_playlist_add "
        print( TAG )
        print( f"{TAG} ..." )
        print( f"{TAG}",  file_list )
        method = 'media_camera_playlist_add'
        camera_list = cam_serials  # ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ]
        file_list   = file_list ;
        data.update(   method = method, params = [ (camera_list, file_list), {} ], id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        # DEL media file from playlist
        TAG = "[3.4] media_camera_playlist_del "
        print( TAG )
        print( f"{TAG} ..." )
        print( f"{TAG}",  file_list )
        method = 'media_camera_playlist_del'
        camera_list = cam_serials  # ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ]
        file_list   = file_list ;
        data.update(   method = method, params = [ (camera_list, file_list), {} ], id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )


        # SET (replace)  media files to playlist
        TAG = "[4.5] media_camera_playlist_set "
        print( TAG )
        print( f"{TAG} ..." )
        print( f"{TAG}",  file_list )
        method = 'media_camera_playlist_set'
        camera_list = cam_serials  # ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ]
        file_list   = file_list ;
        data.update(   method = method, params = [ (camera_list, file_list), {} ], id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        
        #  SET media file from playlist
        TAG = "[4.5] media_camera_playlist_set "
        print( TAG )
        print( f"{TAG} ..." )
        print( f"{TAG}",  file_list )
        method = 'media_camera_playlist_set'
        camera_list = cam_serials  # ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ]
        file_list   = file_list ;
        data.update(   method = method, params = [ (camera_list, file_list), {} ], id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )


        # Sow media file from playlist , after SET 
        TAG = "[4.6] media_camera_playlist_show "
        print(TAG)
        print(f"{TAG} ..." )
        method = 'media_camera_playlist_show'
        data.update(   method = method, params = [ ['2P20A010H3913x01', ['2P20A010H3913x02','2P20A010H3913x02'], ['2P2122730048Dx02','2P2122730048Dx03'], '2P20A010G8978x01' ], {} ]   , id= __gen_id()  ) 
        print( f"{TAG} REQ :-->: { json.dumps(data) } ")
        resp = s.post(URL, json=data )
        print( f"{TAG} RESP:<--:{resp.json()}" )
        if not  handle_if_error(resp):
            result = json.dumps(resp.json()['result'], indent=4)
            print( f"{TAG} result:<--:{result}" )
        
        
        return 0


if __name__ == "__main__":
    sys.exit(main())


