#!/bin/bash

FFMPEG=ffmpeg
VERBOSE="-v quiet"
INFILE=`find ./media/ -maxdepth 1  -type f -iname '*.mp4' | shuf -n1`
OFILE=output/${test_name}/`date +'%Y%m%d%H%M%S'`_`basename $INFILE`_${PID}_'%03d'.jpeg


# -vf select='eq(n\,1)'

$FFMPEG $VERBOSE -an -skip_frame nokey -i ${INFILE}   -vframes 100 -s 640x360 -vsync vfr -qscale:v 2 $OFILE

