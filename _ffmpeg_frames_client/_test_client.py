#!/usr/bin/env python3 

import time
import os
import requests 
import json

def genid():
    return f"{int(time.time()*1000.0)}_{os.getpid()}"

    
URL = "http://192.168.100.4:8088/jsonrpc"
sess = requests.Session()

req = {}
req.update( id = genid(), jsonrpc = '2.0' )



DATADIR=os.path.join( "./test_download_data/" , req['id'] )
if not os.path.exists(DATADIR) :
    os.mkdir(DATADIR)

# print( json.dumps(req) )

kw = {}

import glob 
import random
file_path = random.choice( glob.glob( "./_extract_keyframes/media/*.mp4" ) )

### Upload file 
# Upload FILE / List File / Delete File 
from  urllib.parse import urljoin
url  = urljoin(URL, 'media')
src_fname = file_path
dst_dir   = f'data/{req["id"]}'
dst_url   = urljoin(URL, 'media/' + os.path.basename( src_fname ) )
with open(src_fname,'rb') as r:
    resp = sess.put(dst_url, data=r, params={'dir':dst_dir} )
print( f"REQ  : {resp.request.method} --> {resp.request.url} \n  HEADERS: {resp.request.headers}" )
print( f"RESP : {resp.status_code}    <-- HEADERS: {resp.headers} PAYLOAD: [{resp.text}]" )
### 



# kw.update( filename = os.path.join( 'data', os.path.basename(file_path) ) )

# Get upload filename FROM  reply 
kw.update( filename = resp.json()['dpath']  )

req.update(
      method = "extract_frames"
    , params = [(), kw ]
)


print("request :",  json.dumps(req) )


resp  = sess.post( URL, json=req )
ts    = resp.elapsed.total_seconds()
jresp = resp.json()
print(f"DBG: ELAPSED :{ts}" )
print( json.dumps( jresp['result'] , indent=4 ) )

DL_URL=jresp['result']['URL']
DL_URL = urljoin(URL, DL_URL)
print("DBG: DL_URL:", DL_URL)

res = sess.get( DL_URL , stream=True)
wr = 0
filename = os.path.join( DATADIR, jresp['result']['filename'])
dfile = filename
with open(filename, 'wb') as w:
    while 1:
        d =  res.raw.read(1024)
        if(len(d) == 0):
            break;
        wr+=w.write(d)

print(f"WR :[{wr}] to {dfile} ")

# Remove DATA DIR 
os.unlink(filename)
os.rmdir(DATADIR)

# cmd = f"( cd ./{DATADIR} || exit 1; find ./ -type f -iname '*.tar.gz' -mmin +1 -delete  )"
# os.system(cmd)
