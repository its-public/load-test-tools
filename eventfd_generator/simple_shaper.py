#!/usr/bin/env python3 

import sqlite3 

import os
import sys
import time



start = time.time()
_end = time.time() + 86400

DSN="file:config.sqlite3?mode=rw"
def set_rate(vv):
    v = float(vv)
    with sqlite3.connect(DSN, uri=True) as con:
        q = "UPDATE config SET value=:v  WHERE name = 'rate'" ; pp = {'v':v}
        con.execute(q, pp)

while 1:
    _now = time.time()
    if(_now > _end ) :
        break;

    tm_now = time.gmtime(_now)
    new_value = ( tm_now.tm_min + ( tm_now.tm_sec / 60.0 ) )  / 3.0;
    new_value = round(new_value, 3)
    isotime   = time.strftime("%F %T", tm_now )
    print(f"{isotime} :: simple_shaper : SET rate : {new_value}")
    
    set_rate(new_value)
    
    time.sleep(1)

print("Done ")


