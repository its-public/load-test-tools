CREATE TABLE if not exists config (
     name  text 
    ,value text
    ,type  text
    ,CONSTRAINT uniq__name UNIQUE (name) 
);

INSERT OR IGNORE INTO config (name, value, type ) VALUES ('rate', '0.0', 'float') ;
