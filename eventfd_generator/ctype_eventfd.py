#!/usr/bin/env python3 


import sys
import os
import ctypes 
import time
import signal
import struct 

import json

EFD_CLOEXEC   = 0x80000 
EFD_NONBLOCK  = 0x800 
EFD_SEMAPHORE = 0x1 

import sqlite3 

schema_script = """
CREATE TABLE if not exists config (
     name  text 
    ,value text
    ,type  text
    ,CONSTRAINT uniq__name UNIQUE (name) 
);

INSERT OR IGNORE INTO config (name, value, type ) VALUES ('rate', '0.0', 'float') ;
"""

LOGDIR="./log"
if not os.path.exists(LOGDIR):
    os.mkdir(LOGDIR)

CONFIG_DSN="file:config.sqlite3"
with open('config_schema.sql') as r:
    CONFIG_SQLITE3_DEPLOY_SCRIPT = r.read()
with sqlite3.connect(CONFIG_DSN) as con:
    try :
        con.executescript(CONFIG_SQLITE3_DEPLOY_SCRIPT)
    except sqlite3.IntegrityError as ex:
        pass



def efd_semaphore_open(initval):
    libc = ctypes.cdll.LoadLibrary("libc.so.6")  
    f = libc.eventfd
    fd= libc.eventfd(1, EFD_SEMAPHORE)
    return fd
    
    
def efd_info(fd):
    def _ddecode(k ,v):
        if( k ==  'eventfd-count') : return int(v, 16)
        return int(v,10)

    with open('/proc/{}/fdinfo/{}'.format(os.getpid(), fd) ) as r:
            info = r.read().splitlines(); 
    efd_info = { k.strip(): _ddecode(k.strip(), v.strip())  for k,v in [  ln.split(':') for ln in  [ ln.strip() for ln in info ] ]   }
    return efd_info
    

def worker(fd , worker_id):
    cnt = 0 
    x   = worker_id
    mypid = os.getpid()
    myhost = os.uname()[1]
    while 1:
        cnt +=1
        d = os.read(fd, 8)
        if len(d) == 0:
            break;
        v = struct.unpack ( "@Q", d )
        print("[{}] wid[{:03d}]  WORKER: cnt[{}] V:{}".format(mypid, x, cnt ,v) )
        # Do smth .... 
        _s = time.time()  
        r = os.system(" /bin/bash ./_action.script.sh")
        _e = time.time() 
        gmt = time.gmtime(_s)
        tsf = time.strftime("%Y%m%dt%H00", gmt )
        ts  = time.strftime("%Ft%T%z"    , gmt )
        reportfile = os.path.join(LOGDIR, f"{tsf}__{mypid}_{x}__reports.log" )
        with open(reportfile, "a") as w:
            print(f"{ts}:: host:{myhost} PID:{mypid} WID:{x} :: res:{r} ts:{(_e-_s)*1000.0:.6f}", file=w)
        

    print("[{}] EE: Worker wid:[{:03d}]  Done ".format(mypid, x) )


def avg(it):
    ln = len(it) 
    if(ln==0): return 0;
    return sum(it)/ ln



def _get_rate():
    
    res = 0.0
    with sqlite3.connect(CONFIG_DSN+'?mode=ro', uri=True) as con:
        for row in con.execute("SELECT value,type FROM config WHERE name = 'rate'"):
            value = row[0]
            typename = row[1]
            res = float( value )
        
    return float(res)

def main():
    fd = efd_semaphore_open(0)
    print("FD:", fd)
    
    pids = set()
    NWORKER=120
    for x in range(NWORKER):
        pid = os.fork()
        if(pid == 0) :
            try :
                worker(fd,x)
            except Exception as ex:
                print( " Worker EX:" , str(ex))
            finally:
                print( "WORKER: Exiting ...", file=sys.stderr)
                os._exit(0)
        else:
            pids.add(pid)
        
    max_burst = NWORKER
    pollin    = 0.1
    rate      = 10
    max_underrate = NWORKER * 2
    accum     = 0.0
    iter_cnt  = 0
    _is_underrate = 0
    _s = time.time() - pollin
    verbose = 1
    while(1):
        rate      = _get_rate( )
        iter_cnt += 1
        _n = time.time()
        efd_val = efd_info(fd)['eventfd-count']
        _is_underrate = efd_val > max_underrate
        
        dt = _n - _s 
        vv = (rate * dt) + accum 
        
        ww,accum = int(vv) , vv % 1
        if(verbose >2 ) : print(f"DT: {dt:.3f}  vv:{vv:.3f} ww:{ww:.3f}  accum:{accum:.6f}   efd_val:{efd_val}") ;
        # for x in range(ww):
        if( not _is_underrate ):
            
            for x in range( ww // NWORKER ):
                os.write( fd, struct.pack("@Q", NWORKER) )
            os.write( fd, struct.pack("@Q", int( ww % NWORKER) ) )
        else :
            if ( verbose > 0 ): print(f" --  WW: throttle -- no enough workers  {efd_val} ")
            
        time.sleep(pollin)
        _s = _n


    os.close(fd);
    
    for pid in pids :
        os.kill(pid, signal.SIGINT)
        os.waitpid(pid, 0);

    return 0;

if __name__ == "__main__":
    sys.exit(main())
