#!/bin/bash

TIME_INTERVAL="1"
LOGDIR="./stats"
[[ -d $LOGDIR ]] || { mkdir -p $LOGDIR ; }
[[ -d $LOGDIR ]] || { echo "ERR:[$BASH_SOURCE:$LINENO]  cant create LOGDIR :[${LOGDIR}]" >&2; exit 1; }

HOSTNAME=`hostname`

CLEANUP_LAST=0
CLEANUP_EVERY=86400
CLEANUP_ARGS=" -iname '*_vmstat.log*' -mmin +1 "

COMPRESS_LAST=0
COMPRESS_EVERY=5
COMPRESS_ARGS=" -iname '*_vmstat.log' -mmin +1 "


vmstat $TIME_INTERVAL \
| while read line;
do 
    PREFIX=""
    NOW=`date -u +"%FT%T.%N%z"`
    echo "$line" | grep 'memory' >/dev/null  && { PREFIX="# H1 "; }
    echo "$line" | grep 'cache'  >/dev/null  && { PREFIX="# H2 "; }
    
    OFNAME=`date -u +"%Y%m%d-%H%M00"`_${HOSTNAME}_vmstat.log
    OFILE_NAME="$LOGDIR/$OFNAME"
    echo "${PREFIX}${NOW} $HOSTNAME :: $line" >> $OFILE_NAME;
    

    # -- COMPRESS --------
    if (( SECONDS - COMPRESS_LAST  > COMPRESS_EVERY )) 
    then   
        echo "${BASH_SOURCE}: ${NOW} compress ..." ;
        fcmd="find ${LOGDIR} -type f ${COMPRESS_ARGS}"
        eval ${fcmd} | while read fn ; do gzip -v -9 "$fn"; done 
        echo "${BASH_SOURCE}: ${NOW} compress ... fcmd:[${fcmd}] Done" ;
        COMPRESS_LAST=$SECONDS; 
    fi  

    # -- CLEANUP ---------
    if (( SECONDS - CLEANUP_LAST  > CLEANUP_EVERY )) 
    then   
        echo "${BASH_SOURCE}: ${NOW} cleanup ..." ;
        fcmd="find $LOGDIR -type f $CLEANUP_ARGS -delete ";
        eval ${fcmd};
        echo "${BASH_SOURCE}: ${NOW} cleanup ... fcmd:[${fcmd}] Done" ;
        CLEANUP_LAST=$SECONDS; 
    fi
    
done
