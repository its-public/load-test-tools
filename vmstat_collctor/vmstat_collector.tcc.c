#!/usr/bin/tcc -run 

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>

double d_time(struct timeval *tvp)
{
    struct timeval tv;
    if(!tvp) { gettimeofday(&tv, NULL); tvp = &tv; } 
    return  tvp->tv_sec  +  ( (double)1 /1e6 ) * tvp->tv_usec ;
}

char * time_format_utc(double ts, char *out , int *out_max ) 
{
    
    char b[1024]; int o = 0;
    char tz[1024];
    struct tm   gtm ; time_t tt = (time_t)ts;
    struct tm  *gtmp = gmtime_r( &tt , &gtm);
    
    if(!gtmp) { return 0; }
    o += strftime (b+o, sizeof(b)-1-o ,  "%Y-%m-%d %H:%M:", gtmp );
    o += snprintf (b+o, sizeof(b)-1-o , "%02d.%06d", (int)gtmp->tm_sec, (int)((ts-(int)ts)*1e6) );
    o += strftime (b+o, sizeof(b)-1-o , "%z", gtmp );
    
    int len = 0;
    if(out && out_max)  {
        len += snprintf(out, *out_max, "%s", b);
        // dprintf(2, "DBG: len :%d   out:[%s] out_max:%d\n", len, out, *out_max);
        if (out_max) *out_max = len;
        return out;
    }
    return 0;
}

/*
char *gethostname(char *out) 
{
    char *ret = 0;
    if(!out) return 0;
    FILE *p ;
    int len = 0;
    if( p = popen("hostname", "r") )
    {
        char *line = 0;
        size_t n = 0;
        len = getline(&line, &n, p);
        if(len > 1){  line[len-1] = 0; }
        if(out ) sprintf(out, "%s", line);
        ret = out;
    }
    else {
        out[0] = 0;
    }
    return ret;
}
*/

int main()
{
    char TIME_INTERVAL[]="1";
    char command[4096];
    snprintf(command, sizeof(command)-1, "vmstat %s", TIME_INTERVAL );
    FILE *p1 = popen(command, "r");
    char hostname [1024]; 
    gethostname(hostname, sizeof(hostname)-1 );
    
    
    char *line   = 0 ;
    long line_n  = 0 ;
    int len      = 0 ;
    int cnt      = 0 ;


    while( ( len = getline(&line, &line_n, p1)) ) 
    {
        double _now = d_time(0);
        cnt +=1 ; 
        if(len< 1) break;
        if(len > 1 && line[len-1] =='\n') {  line[len-1] = 0; len -=1; }
        int is_hdr = 0;
        if ( strstr(line, "procs -----------memory") ) {  is_hdr |= 1; }
        if ( strstr(line, "swpd")                    ) {  is_hdr |= 2; }
        
        char tss[1024]; int tssz = sizeof(tss)-1;
        char *tssp =  time_format_utc( _now, tss, &tssz) ;
        
        char wfilename[4096] = {0}; int  wfilename_len; 
        char *LOGDIR ="./stats";
        FILE *ofile = 0;
        {
            int o = 0; time_t tt = (time_t)_now;
            struct tm gtm, *gtmp = gmtime_r( &tt , &gtm);
            struct stat st ; errno=0; int rv= stat(LOGDIR, &st);
            if(rv<0 && errno == 2) // No such file or directory
            {
                char cmd[8192]; int cmd_len = 0; 
                cmd_len = snprintf(cmd, sizeof(cmd)-1,  "mkdir -p %s" , LOGDIR );
                int r = system(cmd);  if(r!=0) { dprintf(2, "ERR failed to execute command [%s] ::%m\n", cmd);   exit(1); }
                break;
            } else {
                if(rv<0) {
                    dprintf(2, "stat[%s] errno:%d ::%m\n", LOGDIR,  errno);                    exit(1);
                }
            }
            
            o += snprintf(wfilename+o , sizeof(wfilename)-o-1,  "%s/", LOGDIR  );
            o += strftime(wfilename+o , sizeof(wfilename)-o-1,  "%Y%m%d-%H%M00", gtmp );
            o += snprintf(wfilename+o , sizeof(wfilename)-o-1,  "_%s_vmstat.log", hostname  );
            
            wfilename_len =o;
        }
        ofile = fopen(wfilename, "a");
        if(ofile) {
            int ww = 0;
            
            if(is_hdr) { ww = fprintf(ofile, "# %s %s  :: %s\n",  tssp, hostname, line ) ; }
            else       { ww = fprintf(ofile, "%s %s :: %s\n",  tssp, hostname, line )    ; }
            
            dprintf(2, "WW: %d: to [%s]\n", ww, wfilename);
        } else {
            dprintf(1, "%s %s :: %s\n",  tssp, hostname, line ) ;
        }
        if(ofile) { fclose(ofile); ofile=0; }
    }

    pclose(p1);
    return 0;    
}
