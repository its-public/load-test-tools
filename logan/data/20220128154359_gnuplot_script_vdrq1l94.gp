#!/usr/bin/gnuplot
    # -----------------------------------------------
    # Automatically generated -----------------------
    # 
    set xdata time
    set grid
    set xtics       180 rotate                # <-- One hour
    set datafile    separator whitespace
    set timefmt     "%s"
    FORMAT_X = "%d %H:%M"
    set format   x  (FORMAT_X)
    set macros 
    set terminal png size 1200,600 enhanced font "/usr/share/fonts/dejavu/DejaVuSans-Bold.ttf,8"
    set xlabel sprintf(  "UTC : Day Time : '%s'", FORMAT_X );


    OUT_FILE_NAME = "/home/leschenko/workspace/tmp/20220128__techtalk_loadtool/logan/images/20220128154359_gnuplot_script_vdrq1l94.gp.png"
    set output    OUT_FILE_NAME
    show grid

    set yrange [0:110]
    set ylabel  "(cpu %)"
    set ytics  10
    set datafile missing "  "
    set size 1,1
    set multiplot 

    set title "Vmstat CPU : host:[alx64sms] "   font "/usr/share/fonts/dejavu/DejaVuSans-Bold.ttf, 12" 
    plot \
            "/home/leschenko/workspace/tmp/20220128__techtalk_loadtool/logan/data/20220128154359_data_1e5l7zxf"  using 1:($4)      title "CPU-Idle" with lines lw 2 lc rgb 'green' axis x1y1, \
            "/home/leschenko/workspace/tmp/20220128__techtalk_loadtool/logan/data/20220128154359_data_1e5l7zxf"  using 1:($2)      title "CPU-Sys"  with lines lw 2 lc rgb 'red'   axis x1y1, \
            "/home/leschenko/workspace/tmp/20220128__techtalk_loadtool/logan/data/20220128154359_data_1e5l7zxf"  using 1:($3)      title "CPU-User" with lines lw 2 lc rgb 'blue'  axis x1y1 \
            ;

    system( sprintf("echo IMAGE_FILE: %s", OUT_FILE_NAME )  );
    