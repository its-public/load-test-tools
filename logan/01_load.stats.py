#!/usr/bin/env python3 

import os
import time
import sqlite3 
import glob

VMSTATS_PATH="../vmstat_collctor/stats/" # 20220128-072100_alx64sms_vmstat.log.gz"
VMSTATS_DSN="file:vmstats.sqlite3?mode=rw"



# Data Sample : 
# 2022-01-28T12:28:00.272545007+0000 alx64sms :: 6  0 8747520 5852016    124 13825928    0    0     0   488 28114 54824 15  3 82  0  0

vmstats__hh_format = "r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st".split()

sqlite3_deploy_script = ( """
    CREATE table if not exists vmstats 
    (
          "timestamp"   text    NOT NULL  -- e.g : 2022-01-28T04:58:00.299520738+0000 
        , "utcTime"     bigint  NOT NULL -- e.g : Unix timestamp 
        , "hostname"    text    NOT NULL  -- e.g : alx64sms\n""" + 
    "\n".join(  
     [ f"""        , "{k}"   integer""" for k in vmstats__hh_format ] )
    + """
        , PRIMARY KEY("timestamp", "hostname")
    );
    """
)


print("SQL DEPLOY SCRIPT:", sqlite3_deploy_script, "\n ---------------")



def _parse_timestamp__tofloat(tss):
    timestamp = tss
    date_, time_ = timestamp.split("T")
    time_sec,time_subsec = time_.split('.',1)
    if '+' in time_subsec  :
        time_subsec, zone = time_subsec.split("+",1); 
        zone = "+" + zone[0:2] + ":" + zone[2:4]
    if '-' in time_subsec  :
        time_subsec, zone = time_subsec.split("-",1);
        zone = "-" + zone[0:2] + ":" + zone[2:4]
    
    subsec = float("0." + time_subsec)            # float subsecond part 
    timestamp_ = date_ + "T" + time_sec 
    utcTime = time.mktime( time.strptime(timestamp_,  "%Y-%m-%dT%H:%M:%S") ) + subsec
    local_zoffset = time.mktime( time.gmtime(0) ) # -10800.0 # FIX Localtime for time.mktime()
    utcTime -= local_zoffset
    return utcTime



def _parse_line(ln):
    dd = {} 
    # print("RR:", ln)
    header, values = ln .split("::") 
    # Parse  Record header    # 2022-01-28T04:58:00.299520738+0000 alx64sms ::
    header = header.rstrip()
    # print(f"Header:[{header}]")
    
    timestamp, hostname  = header.split()
    dd.update( timestamp = timestamp, hostname=hostname )
    
    # parse timestamp 
    dd.update( utcTime   = _parse_timestamp__tofloat(timestamp) )
    
    # Parse Values 
    hh = vmstats__hh_format
    vv = values.split()
    ddv = dict( zip(hh,map(int,vv)) ) 
    
    dd.update( ddv )
    return dd    
    
    

def sqlite3__perform_insert(cur, dd) :
    
    q  = "INSERT OR REPLACE into vmstats (" 
    q += """ "timestamp", "hostname", "utcTime" , """
    q +=      ",".join( [ f'"{k}"' for k in vmstats__hh_format ] ) 
    q += ") \n  VALUES( "
    q +=          ':timestamp'
    q +=        ', :hostname '
    q +=        ', :utcTime, '    
    q +=     ", ".join( [ f':{k}' for k in vmstats__hh_format]  ) 
    q += ");"
    
    # print("Q:", q)

    pp = dict(dd)
    try :
        return cur.execute(q, pp)
    except Exception as ex:
        print(f"SQL ERR: Q:{q} pp:{pp}")
        raise 
    






def do_process_stat_file(stat_file):
    
    n,e = os.path.splitext(stat_file)
    pipe_cmd = f"zcat {stat_file}"  if(e == '.gz') else f"cat {stat_file}"; 

    ntry = 0
    while 1:
        ntry +=1
        if(ntry >2) :
            raise Exception("Cant deploy DB")
        try:
            with sqlite3.connect(VMSTATS_DSN, uri=True) as con:
                con.executescript(sqlite3_deploy_script)
            break
        except sqlite3.OperationalError as ex:
            if 'unable to open database file' in str(ex) :
                with sqlite3.connect(VMSTATS_DSN+'c', uri=True) as con:
                    continue 
            else :
                raise
            
    ins = 0
    with sqlite3.connect(VMSTATS_DSN, uri=True) as con:
        cur = con.cursor()
        with os.popen(pipe_cmd) as p1 :
            for ln in p1:
                ln = ln.rstrip()
                ln = ln.split("#",1)[0]
                if(len(ln.strip()) <= 0 ):
                    # skip 
                    continue
                try :
                    dd = _parse_line(ln)
                except Exception as ex:
                    print(f" EEEE: PARSE FAILED : ln[{ln}]   :: ex:{ex}" )
                    raise 
                    continue ;
                sqlite3__perform_insert(con , dd)
                ins+=1
    return ins # ins -- number of inserted records 


_search_mask = VMSTATS_PATH + '*.gz'
print(f"GLOB: _search_mask: {_search_mask}")
statfiles = glob.glob(_search_mask)
for stat_file in statfiles:
    print(f"PROCESSING file :{stat_file} ... ", end = "")
    inserted  = do_process_stat_file(stat_file)
    print(f" [{inserted}]")
