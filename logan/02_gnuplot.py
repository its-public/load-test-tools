#!/usr/bin/env python3 

import os
import sys
import time
import calendar
import sqlite3
import tempfile



def query_vmstat_cpu__data_set (start, end , host, aggregate_seconds=1, context= {}) :
    start_ts = start
    end_ts   = end  

    VMSTAT_DSN = context['VMSTAT_DSN']
    OUT_file_prefix = context['OUT_file_prefix']    

    with sqlite3.connect(VMSTAT_DSN, uri=True) as con:
        q = """
        SELECT utcTime_int as ts, AVG(sub.sy), AVG(sub.us), AVG(sub.id) 
        FROM
        ( 
            SELECT CAST( utcTime as INT4 ) as utcTime_int, sy, us, id
            FROM vmstats 
            WHERE "hostname" = :host  
               AND utcTime BETWEEN :start_ts  AND :end_ts
        )  as sub
        GROUP BY utcTime_int/{aggregate_seconds}
        ORDER BY ts
        """.format( aggregate_seconds = int(aggregate_seconds) )
    
        pp = {'start_ts': start_ts , 'end_ts':end_ts, 'host':host }
        cur = con.execute(q, pp)
    
        o = tempfile.mkstemp(dir="./data", prefix=OUT_file_prefix+"_data_" )
        with os.fdopen(o[0], "wt") as w:
            for row in cur:
                print( "\t".join( str(val) for val in row ), file=w  )
    
        return o[1]


def gen_gnuplot_script__vmstat_cpu(dataset_file_name, context={}):
    FONT_FILE = "/usr/share/fonts/dejavu/DejaVuSans-Bold.ttf"
    gnuplot_template = \
    """#!/usr/bin/gnuplot
    # -----------------------------------------------
    # Automatically generated -----------------------
    # 
    set xdata time
    set grid
    set xtics       180 rotate                # <-- One hour
    set datafile    separator whitespace
    set timefmt     "%s"
    FORMAT_X = "%d %H:%M"
    set format   x  (FORMAT_X)
    set macros 
    set terminal png size 1200,600 enhanced font "{FONT_FILE},8"
    set xlabel sprintf(  "UTC : Day Time : '%s'", FORMAT_X );


    OUT_FILE_NAME = "{IMAGE_FILE}"
    set output    OUT_FILE_NAME
    show grid

    set yrange [0:110]
    set ylabel  "(cpu %)"
    set ytics  10
    set datafile missing "  "
    set size 1,1
    set multiplot 

    set title "{TITLE}"   font "{FONT_FILE}, 12" 
    plot \\
            "{DATAFILE}"  using 1:($4)      title "CPU-Idle" with lines lw 2 lc rgb 'green' axis x1y1, \\
            "{DATAFILE}"  using 1:($2)      title "CPU-Sys"  with lines lw 2 lc rgb 'red'   axis x1y1, \\
            "{DATAFILE}"  using 1:($3)      title "CPU-User" with lines lw 2 lc rgb 'blue'  axis x1y1 \\
            ;

    system( sprintf("echo IMAGE_FILE: %s", OUT_FILE_NAME )  );
    """
    
    OUT_file_prefix = context['OUT_file_prefix']
    HOST            = context['HOST']
    START           = context['START']
    END             = context['END']    
    IMAGE_DIR       = context['IMAGE_DIR']
    
    o = tempfile.mkstemp( dir="./data", prefix=OUT_file_prefix+"_gnuplot_script_",  suffix=".gp" )
    image_file = os.path.basename(o[1]) + '.png'
    image_file = os.path.join( IMAGE_DIR, image_file  ) 
    image_file = os.path.abspath(image_file)

    gp_script = gnuplot_template.format(
        IMAGE_FILE = image_file
        , DATAFILE = dataset_file_name
        , HOST     = HOST
        , START    = START
        , END      = END
        , FONT_FILE = FONT_FILE
        , TITLE    = f"Vmstat CPU : host:[{HOST}] "
    ) 

    with os.fdopen(o[0], 'w' ) as w:
        w.write(gp_script)

    return o[1]



def gnuplot_execute( script_filename, context={} ):
    cmd = f"gnuplot {script_filename}"
    out = []
    with os.popen( cmd ) as p1:
        for ln in p1:
            out += [ln.rstrip()]
    image_file = None
    for ln in out:
        if 'IMAGE_FILE:' in ln :
            image_file = ln.split(":",1)[-1].strip()

    return image_file




def main():

    context = {}
    # SETUP PArams ----------------------------------------------------------------------------------------
    
    context.update ( 
         NOW  = time.time()
        ,OUT_file_prefix = f"{time.strftime('%Y%m%d%H%M%S')}"
        ,INTERVAL = {
             'START' : "2022-01-28 08:00:00+0000"
            ,'END'   : "2022-01-28 12:00:00+0000"
        }
        ,HOST         = "alx64sms"
        ,VMSTAT_DSN   = "file:vmstats.sqlite3?mode=ro"
        ,AGGREGATE_TIME=60  # 20
        ,IMAGE_DIR    = "./images"

    )
    context.update ( 
        #   START = time.mktime( time.strptime( context['INTERVAL']['START'] , '%Y-%m-%d %H:%M:%S%z' ) )
        # , END   = time.mktime( time.strptime( context['INTERVAL']['END']   , '%Y-%m-%d %H:%M:%S%z' ) )

          START = calendar.timegm( time.strptime( context['INTERVAL']['START'] , '%Y-%m-%d %H:%M:%S%z' ) )
        , END   = calendar.timegm( time.strptime( context['INTERVAL']['END']   , '%Y-%m-%d %H:%M:%S%z' ) )

    )
    
    print(f"START : {context['START']:.3f} INTERVAL[START] : {context['INTERVAL']['START']}")
    print(f"END   : {context['END']:.3f} INTERVAL[END]   : {context['INTERVAL']['END']}"  )

    # -----------------------------------------------------------------------------------------------------


    # 1: Query DB for DATA set && write to DATA_FILE
    #        returns : generated datafile name  
    dataset_file_name = query_vmstat_cpu__data_set ( 
          context['START']
        , context['END'] 
        , context['HOST']
        , aggregate_seconds=context['AGGREGATE_TIME']
        , context=context 
    ) ;
    print  ( f"DATASET FILE  : {dataset_file_name}" )
    
    # 2: Generate gnuplot script for build a graph
    #        returns : generated script file name  
    gnuplot_script_file = gen_gnuplot_script__vmstat_cpu  ( dataset_file_name , context=context);
    print  ( f"GNUPLOT FILE  : {gnuplot_script_file} ")
    
    # 3: Execute gnuplot script -- generate image file returns image file name 
    image_file_name = gnuplot_execute( gnuplot_script_file , context=context );
    print  ( f"GNUPLOT IMAGE : {image_file_name} " )

    return 0


if __name__ == "__main__":
    sys.exit(main())
