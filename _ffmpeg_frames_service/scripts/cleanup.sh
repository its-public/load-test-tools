#!/bin/bash

[[ -z "$MEDIA_DIR" ]] && MEDIA_DIR=./upload

(
    cd "${MEDIA_DIR}" || exit 1;
    find ./ -type f -mmin +1 -iname '*.jpeg'    -delete # 
    find ./ -type f -mmin +1 -iname '*.tar.gz'  -delete # 
    find ./ -type f -mmin +1 -iname '*.mp4'     -delete # 
    find ./ -type d -mmin +1 -iname 'tmp.*'     -empty       -delete
    find ./ -type d -mmin +1  -empty       -delete
) || exit 1
