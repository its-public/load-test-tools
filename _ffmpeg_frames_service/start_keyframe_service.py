#!/usr/bin/env python3 

import os,time,sys
import bottle
import threading 

import logging
import logging.handlers

LOGDIR="./log"
TMPDIR="./tmp"
os.makedirs(LOGDIR, exist_ok=True)

logging.basicConfig (
        level=logging.DEBUG # level=logging.INFO 
        #  threadName
      , format  = "%(asctime)s [PID:%(process)-5d] [%(levelname)5s] [%(module)12s:%(lineno)-3d] %(message)s" 
      , datefmt = '%Y-%m-%dT%H:%M:%S%z'
      , handlers = [
            logging.handlers.TimedRotatingFileHandler(os.path.join(LOGDIR, 'sim_ctl_server.log'), when='H', backupCount=24 ) 
          , logging.StreamHandler()
       ] 
    )

app = application = bottle.Bottle()
from plugins import bottle_jsonrpc

SIM_AUTO_PREFIX=os.environ.get('SIM_AUTO_PREFIX', os.path.realpath('..'))
os.environ['SIM_AUTO_PREFIX'] = SIM_AUTO_PREFIX

sys.path.insert(0, os.path.join(SIM_AUTO_PREFIX)  )
from tools.utils import log_hdr

def mass_format_exc():
    from traceback import format_exc
    
    stacktrace = format_exc()
    prefix = "\n%-15s%s" % ("",'-- ')
    return  prefix + prefix.join( stacktrace.split('\n') )


jrpc = bottle_jsonrpc.register('/jsonrpc', app=app)

# Media -- UPLAOAD ------------------------------------------------------------------------

import tempfile
import hashlib

def is_subdir(p1, p2):
    p1, p2 = os.path.realpath(p1), os.path.realpath(p2)
    return p1 == p2 or p1.startswith(p2+os.sep)


MEDIA_DIR="./upload"

@app.route("/media"                , method=['GET','PUT', 'POST','DELETE'] )
@app.route("/media/"               , method=['GET','PUT', 'POST','DELETE'] )
@app.route('/media/<filename:path>', method=['GET','PUT', 'POST','DELETE'] )
def media(filename=None):

    # MEDIA_DIR = "../media/data"
    request   = bottle.request
    
    try :
        if 1:
            print(f"DBG: request.chunked        : [{request.chunked}]"        ) 
            print(f"DBG: request.content_type   : [{request.content_type}]"   )     
            print(f"DBG: request.content_length : [{request.content_length}]" ) 
            print(f"DBG:        request.method  : [{request.method}]") 
            print(f"DBG:              filename  : [{filename}]"      )
            print(f'DBG:          request.path  : [{request.path}]'  )
        if 0 :
            print(f"DBG:        request.params  : [{dict(request.params)}]")
            print(f"DBG:        request.files   : [{request.files.keys()}]")
        if 1:            
            print(f"DBG:         request.query  : [{dict(request.query)}]" )
            print(f"DBG:  request.query_string  : [{request.query_string}]" )
            print(f"DBG:       request.content  : [{request.content_type}]" )
            print(f"DBG:           request.url  : [{request.url}]" )
    except :
        ex = mass_format_exc()
        print("DBG: got except :", ex )
        return bottle.HTTPError(status=500, body=f"Internal server error EX:\n{ex}\n")
        
    try : 
        # Check filename / "dir=" param
        if request.method in ( "PUT", "GET" ) :
            dirname = request.query.get('dir')
        else :
            dirname = request.params.get('dir')
            
            
        if dirname !=None and  os.path.isabs( dirname ):
            return  bottle.HTTPError(status=400, body=f"Restricted path:[dir={dirname}] abspath is not allowed" )
        if filename != None and os.path.isabs( filename ):
            return  bottle.HTTPError(status=400, body=f"Restricted path:[{filename}] abspath is not allowed" )

        out = []
        # Method GET -----------------------------------------------------------------
        if   request.method == 'GET' : 
            # List media Files 
            logging.debug(f"DBG : filename:{filename}  dirname:{dirname}" )
            if dirname == None and filename != None : 
                # Single file
                if not is_subdir(  os.path.join(MEDIA_DIR, filename) , MEDIA_DIR ):
                    return  bottle.HTTPError(status=404, body=f"Restricted path :[{filename}] " )
                dpath = filename
            else :
                # dpath = os.path.join( filename  if filename !=None  else '' , dirname  if dirname  !=None  else '' ) 
                if filename != None :
                    dpath = os.path.join(  dirname, filename  ) 
                else :
                    if dirname != None :  
                        dpath = os.path.join( dirname )
                    else:
                        dpath = None
                    
            if dpath!=None and len(dpath) == 0 :
                dpath  = None 
            logging.debug(f"DBG : dpath :{dpath}")
            if os.path.isdir( os.path.join( MEDIA_DIR, dpath ) ):
                # List dir 
                flist   = _list_media_files(MEDIA_DIR, dpath=dpath )
                out     = flist
            else :
                # Send single file 
                return bottle.static_file(dpath, root=MEDIA_DIR,  download=True )
                
            return out
        
        # Method PUT -----------------------------------------------------------------
        elif request.method == 'PUT':
        
            jpath = [ pp for pp in [ MEDIA_DIR, request.query.get('dir'), filename ]  if pp != None ]
            dest_fname = os.path.join( *jpath ) 
            dest_fname = os.path.normpath(dest_fname)
            if not is_subdir(dest_fname, MEDIA_DIR):
                dpath = os.path.join( *(jpath[1:]) ) 
                return bottle.HTTPError( status=400, body=f"Invalid destination path:[{dpath}]" )
            logging.debug(f"method PUT : dest_fname :{dest_fname}" )
            
            
            dest_dir = os.path.dirname(dest_fname)
            if not os.path.exists(dest_dir) :
                os.makedirs(dest_dir)
            
            fd, dfname = tempfile.mkstemp(suffix='.part', prefix=os.path.basename(dest_fname)+'.', dir=os.path.dirname(dest_fname), text=False )
            try :
                logging.debug(f" temp file:[fd:{fd}  dfname:{dfname}]")
                fname           = dfname 
                f               = request['wsgi.input']
                content_length  = int(request.content_length)
                wr              = 0
                bs              = 64*1024
                with os.fdopen(fd, 'wb') as w:
                    cnt = 0
                    while 1:
                        cnt+=1
                        rd = bs if (content_length >= bs) else content_length
                        d = f.read( rd )
                        # logging.debug (f"PID:[{os.getpid()}] D: len ({len(d)}) ::{d[:15]}")
                        if len(d) == 0:
                            break;
                        wr += w.write(d)
                        content_length -= len(d)
                        
                        if(cnt%100 == 0): time.sleep(0.01)
                        
                logging.debug( f"[PID:{os.getpid()}] Got File :{fname}  : WR:{wr} " )
                
                oldname = dfname
                newname = dest_fname
                if os.path.exists(newname):
                    logging.debug(f"Overwriting media file :[{newname}]")
                logging.debug(f"Rename :{oldname} --> {newname}")
                os.rename( oldname, newname )
            
                with open(newname, 'rb') as r:
                    rd = f.read(8192)
                    md5hash_8k = hashlib.md5( r.read(8*1024) ).hexdigest()
                return bottle.HTTPResponse(status=200, body={
                           'MEDIA_DIR'  : MEDIA_DIR 
                         , 'dpath'      : os.path.join( *(jpath[1:]) )  
                         , 'md5hash_8k' : md5hash_8k
                         , 'size'       : os.stat(newname).st_size
                     })
            finally :
                logging.debug(f"Unlink temp file:[{dfname}]")
                if os.path.exists(dfname) :
                    os.unlink(dfname)

            return bottle.HTTPError(status=500, body=f"Method handle {request.method} not implemented")
            pass

        # Method POST -------------------------------------------------------------------------------
        elif request.method == 'POST' :
            return  bottle.HTTPError(status=500, body=f"Method handle {request.method} not implemented")

        # Method DELETE -------------------------------------------------------------------------------
        elif request.method == 'DELETE':
            return bottle.HTTPError(status=500, body=f"Method handle {request.method} not implemented")
        else :
            return bottle.HTTPError(status=404, body=f"Unsupported method:{request.method}")

    except :
        ex = mass_format_exc()
        print("DBG: got except :", ex )
        raise bottle.HTTPError( status=500, body=f"Internal server error Ex:\n{ex}\n" )
    
    return 'OK'




# Methods ------------------------------------------------------------
def method__echo1(*a, **kw):
    if(len( kw.keys()) == 0):
        return a
    return (a, kw)
    
    
def method__extract_frames(*a):

    
    from  urllib.parse import urljoin
    
    log  = []
    result = {'_log' : log }
    
    aa = a[0]
    kw = a[1]

    for a in aa :
        log += [f"DD: args : {a}"]
    for k in kw :
        log += [f"DD: kwargs : {k}={kw[k]}"]
    # ---------------------------------------

    filename = kw['filename']

    json_rpc_id = bottle.request.json['id']
    print("DBG: BOTTLE.request.json:",  bottle.request.json )
    
    # raise Exception("HALT")
    
    file_path = os.path.join(MEDIA_DIR, filename)
    dn = os.path.dirname ( file_path )
    bn = os.path.basename( file_path )
    
    with os.popen( f"mktemp -d -p {TMPDIR}" ) as p1:
        dtemp = "".join(p1.readlines()).strip()
    
    
    
    ffcommand = " ffmpeg "
    VERBOSE   = "-v quiet"
    INFILE    = file_path
    OFILE     = os.path.join( dtemp,   f'{bn}_%03d.jpeg' )

    ffcommand  = f"ffmpeg {VERBOSE}"
    ffcommand += f" -an -skip_frame nokey "
    ffcommand += f" -i {INFILE} "
    ffcommand += f" -vframes 100 -s 640x360 -vsync vfr -qscale:v 2 {OFILE}"

    log += [f"DD: ffmpeg command :[{ffcommand}]"]

    # -vf select='eq(n\,1)'
    # $FFMPEG $VERBOSE -an -skip_frame nokey -i ${INFILE}   -vframes 100 -s 640x360 -vsync vfr -qscale:v 2 $OFILE

    o,e = tempfile.mkstemp(dir=TMPDIR), tempfile.mkstemp(dir=TMPDIR)
    print(f"DBG: o/e: {o}/{e}")
    cmd = " ( {ffcommand} ) >{o} 2>{e} ".format(
          ffcommand = ffcommand
        , o = o[1]
        , e = e[1]
    ) 
    res = os.system(cmd)
    result['rescode'] = res
    with os.fdopen(o[0], "rt") as r:
        log += [ f"EE: o:{ln.rstrip()}" for ln in r  ]
        os.unlink( o[1] )
    with os.fdopen(e[0], "rt") as r:
        log += [ f"EE: e:{ln.rstrip()}" for ln in r  ]
        os.unlink( e[1] )
    res = os.system(f' mv -v  {dtemp}  "{MEDIA_DIR}"')
    if res == 0 :
        # ok 
        log += ["OK : dtemp:{dtemp}" ]
        
        # compress 
        bn = os.path.basename( file_path )
        root,ext = os.path.splitext(bn)
        tarname = root + "_frames.tar.gz"
        
        print(f"TARNAME:{tarname}")
        tar_cmd = f" ( cd {os.path.join(MEDIA_DIR, os.path.basename(dtemp))} && tar -zcf {tarname} ./* )"
        res = os.system(tar_cmd)
        log += [f"II: TAR : {tar_cmd}  :{res}" ]

        tarpath = os.path.join( os.path.basename(dtemp), tarname )

        with  os.popen(f"md5sum {os.path.join(MEDIA_DIR, tarpath)}") as p1:
            md5tar = " ".join( [ ln.rstrip() for ln in p1 ] ).strip()
        log += [f"II: TAR : ms5sum : {md5tar}"]

        result["URL"] = os.path.join( "/media", tarpath )
        result["filename"] = os.path.basename( tarpath )
    
    # remove 
    os.unlink( file_path )

    cmd = f" find ./{MEDIA_DIR} -type f -wholename '*/tmp.*.jpeg' -or -name '*.tar.gz' -mmin +1 -delete "
    os.system(cmd)
    cmd = f" find ./{MEDIA_DIR} -type d -empty -delete "
    os.system(cmd)
    
    # ---------------------------------------
    # log += ["EE: Not implemented "]
    return result

methods  = {
      'echo'            : method__echo1
    # --------------------------------
    , 'extract_frames'  : method__extract_frames
}
jrpc.methods = methods 



def do_maintanance_job(context={}):
    cmd = "scripts/cleanup.sh"
    rv = os.system(cmd)


def worker_scheduler(logger, context={}):
    context = {}
    if context.get('logger') == None:
        context.update( logger  = logger )
    while 1:
        time.sleep(10)
        try :
            logger.debug("Start Maintainance job")
            do_maintanance_job(context=context)
            logger.debug("END Maintainance job")
        except Exception as ex:
            logger.error("EX: ", mass_format_exc())
            time.sleep(1)
    pass




def main():
    LOGDIR = './log'
    
    if not os.path.exists(LOGDIR):
        os.makedirs(LOGDIR)
    
    # logger = setup_logger(logdir=LOGDIR)
    logger = logging.getLogger()
    
    # Start scheduler -- _STARTED - protects from debug watchdog double start
    if os.environ.get("STARTING",'') == '' :
        # Start Scheduler thread -------
        sched_th = threading.Thread(target=worker_scheduler, args=(logger,))
        sched_th.daemon=True
        sched_th.start()
        os.environ['STARTING'] = str(os.getpid())
    # 
    
    
    LISTEN = ( '0.0.0.0', 8088 )
    logger.debug(f"LISTEN:{LISTEN}")

    server = 'wsgiref' 
    try:
        from cheroot import wsgi
        server = 'cheroot'
    except :
        pass 
        
    bottle.run (
              reloader = True
            , debug    = True
            , app      = app
            # , server = 'meinheld'
            , server   = server
            , host     = LISTEN[0]
            , port     = LISTEN[1]
            )
    return 0



if __name__ == "__main__":
    sys.exit(main())
