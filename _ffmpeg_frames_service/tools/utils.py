#!/usr/bin/env python3 

def log_hdr():
    import inspect,os,time
    
    def _isotime( ts=None , how='full', gmt=True, relative=True, relativefrom_last=True):
        global __start
        global __prev
        
        try:
            if __start == None:
                __start = time.time()
                __prev  = __start
        except NameError:
            __start = time.time()
            __prev  = __start
        
        if ( relative==True ):
            _now = time.time()
            if ( relativefrom_last==True ):
                res  =  "TT+{0:09.4f}".format( _now - __prev ) 
                __prev = _now
            else:
                res =  "TT+{0:09.4f}".format( _now - __start ) 
            return res
        
        if(ts == None): ts = time.time()
        tm = time.gmtime(ts) if(gmt) else time.localtime(ts)
        if  (how == 'short'):
            return time.strftime("%Y%m%dT%H%M%S",  tm )
        elif(how == 'full'):
            mag, fraq = ("%.6f" % ts).split('.',1)
            return time.strftime("%Y-%m-%dT%H:%M:%S"+'.'+fraq + "%z" ,  tm )
        elif(how == 'shortms'):
            mag, fraq = ("%.6f" % ts).split('.',1)
            return time.strftime("%Y%m%dT%H%M%S"+'.'+fraq + "%z" ,  tm )

    #caller_frame =     # 0 represents this line # 1 represents line at caller
    frame = inspect.stack()[1][0]
    info  = inspect.getframeinfo(frame)
    return "{} [pid:{:>3}] [{}:{:>3}]".format(#info.function    # __FUNCTION__ -> Main
          _isotime(how='shortms')
        , os.getpid()
        , os.path.basename(info.filename)        # __FILE__     -> Test.py
        , info.lineno                            # __LINE__     -> 13
    )
