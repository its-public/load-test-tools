
# Decorator ------------------------------
def fork_wrap(*a, **kw):
    
    #print(log_hdr(), "DBG: frok_wrap: A:{}".format(a))
    #print(log_hdr(), "DBG: frok_wrap:KW:{}".format(kw))
    logger = logging.getLogger()
    logger.debug("DBG: frok_wrap: A:{}".format(a)  ) 
    logger.debug("DBG: frok_wrap:KW:{}".format(kw) )
    
    proc = a[0]

    def _decorated( *aa, **kww ):
        # print(log_hdr(), "fork_wrap.__decorated: AA:{}  KWW:{}".format(aa, kww) )
        logger.debug( "fork_wrap.__decorated: AA:{}  KWW:{}".format(aa, kww) )
        
        pipe = os.pipe()
        pid = os.fork()
        if (pid == 0):
            os.close(0)
            # os.closerange(3,pipe[0])
            _pid = os.getpid()
            # os.system('ls -la /proc/{}/fd'.format(_pid))
            # os.system('lsof -np {}'.format(_pid))
        
            # print("DBG: call proc({}) A:{}  KW:{}".format(proc, aa, kww) )
            logger.debug( "DBG: call proc({}) A:{}  KW:{}".format(proc, aa, kww) )
            
            code = 1
            try :
                # print(log_hdr(), "DBG: PROC:{}".format(proc) , file=sys.stderr)
                logger.debug( "DBG: PROC:{}".format(proc) )
                
                r = proc( *aa, **kww )
                
                # print(log_hdr(), "DBG: res:{}".format(r) , file=sys.stderr)
                logger.debug( "DBG: res:{}".format(r) )
                
                # out = { 'code':0, 'result': json.dumps(r), 'error': None }
                out = { 'code':0, 'result': r, 'error': None }
                res = json.dumps(out)
                os.write(pipe[1], bytes(res,'utf-8'))
                code = 0
            # os.close(pipe[1])
            except Exception as ex:
                # print(log_hdr(),"EX: {} :{}".format(ex, mass_format_exc()), file=sys.stderr)
                logger.debug(  "EX: {} :{}".format(ex, mass_format_exc()) )
                r   = { 'error':1, 'message': [ ln.strip() for ln in mass_format_exc().splitlines() ] }
                out = {  'code':1, 'result':None , 'error': r }
                res = json.dumps(out) 
                os.write(pipe[1], bytes(res,'utf-8'))
                code = 1
            finally:
                # print(log_hdr(), "FINALLY: CODE:{}".format(code) , file=sys.stderr )
                # logger.debug( "FINALLY: CODE:{}".format(code)  )
                os.close(pipe[1])
                os._exit(code)
        
        # print("DBG: read from child pipe ...")
        logger.debug("DBG: read from child pipe ...")
        data = b''
        while (1):
            d = os.read( pipe[0], 4096 )
            # print(log_hdr(), "RX:", d)
            logger.debug( "RX: {}".format( d ) )
            if(len(d) == 0):
                break;
            data += d
            if (len(d) < 4096) : break;
            
        # print("DBG: read from child pipe ... got :{}".format(data))
        logger.debug("DBG: read from child pipe ... got :{}".format(data) )
        
        cnt = 0
        while 1:
            code = os.waitpid(pid, os.WNOHANG)
            # print("WAITPID [{}]: code:{}".format(pid, code) )
            logger.debug("WAITPID [{}]: code:{}".format(pid, code) )
            if(code!=(0,0)):
                break;
            time.sleep(0.05)
        
        os.close(pipe[0])
        os.close(pipe[1])
        dd = data.decode('utf-8')
        jres = json.loads( dd )
        
        # print("DBG: jres:{}".format(jres))
        logger.debug("DBG: jres:{}".format(jres))
        if type(jres) == dict and jres.get('error', None) != None:
            raise Exception(jres)

        return jres['result']
        
    return _decorated
    
